const arg0 = parseInt(process.argv[2]);

if(isNaN(arg0)){
    console.log("Please enter valid a number");
    return;
}
let sum = 0;
for(let i = 0; i <= arg0; i++)
    sum += ((i % 3 === 0) || ( i % 5 === 0)) ? i : 0;

console.log(sum);
