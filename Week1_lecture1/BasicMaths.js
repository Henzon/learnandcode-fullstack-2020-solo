const arg0 = parseInt(process.argv[2]);
const arg1 = parseInt(process.argv[3]);

if(isNaN(arg0) || isNaN(arg1)){
    console.log("Please enter 2 numbers");
    return;
} 
console.log("Numbers '" + arg0 + "' '" + arg1 + "' have the following math results")
console.log("Sum :: " + (arg0 + arg1));
console.log("Diff :: " + (arg0 - arg1)); 
console.log("Fraction :: " + (arg0 / arg1));
console.log("Product :: " + (arg0 * arg1));
console.log("Average :: " + ((arg0 + arg1) * 0.5));
