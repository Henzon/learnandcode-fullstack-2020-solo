const arg0 = parseInt(process.argv[2]);

if(isNaN(arg0)){
    console.log("Please enter a valid number.");
    return;
}
if(arg0 < 1) return;

let fac = 1;
for(let i = 2; i <= arg0; i++)
    fac *= i;

console.log(arg0 + "! is :: " + fac);
