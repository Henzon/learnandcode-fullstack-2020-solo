const arg0 = parseInt(process.argv[2]);

if(isNaN(arg0)){
    console.log("Please enter a number");
    return;
} 
let sum = 0
for(let i = 1; i <= arg0; i++)
    sum += i;

console.log(arg0 + "! = " + sum);
