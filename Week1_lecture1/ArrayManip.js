const arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22];

const div3 = arr.filter((value) => {return !(value % 3)});
console.log(div3);

const mul2 = div3.map((value) => {return value << 1});
console.log(mul2);

const total = mul2.reduce((sum, num) =>{return sum + num }, 0);
console.log(total);
