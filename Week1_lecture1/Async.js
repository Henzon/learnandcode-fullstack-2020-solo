const getValue = function() {
   return new Promise((res, rej) => {
       setTimeout(() => {
           res({ value: Math.random() });
       }, Math.random() * 1500);
   });
};

Promise.all([getValue(), getValue()]).then((promises) => {
    console.log("PROMISE : Value 1 is " + promises[0].value +
               " and value 2 is " + promises[1].value);
})

const AsyncPrint = async function(){    
    const val0 = await getValue();
    const val1 = await getValue();
    console.log("ASYNC : Value 1 is " + val0.value +
                " and value 2 is " + val1.value);
}
AsyncPrint();
