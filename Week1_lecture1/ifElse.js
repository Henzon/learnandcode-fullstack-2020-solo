const arg0 = parseInt(process.argv[2]);
const arg1 = parseInt(process.argv[3]);


if(isNaN(arg0) || isNaN(arg1)){
    console.log("Please enter 2 numbers");
    return;
} 
console.log((arg0 === arg1) ? "a and b are equal" : (arg0 > arg1) ? "a is greater" : "b is greater");


const password = "hello world"
if(arg0 === arg1
   && ((process.argv[4] === password) //If password was entered with quotation marks
       || (process.argv[4] + " "+ process.argv[5]) === password)) //if password was entered without quotation marks.
console.log("yay, you guessed the password");
