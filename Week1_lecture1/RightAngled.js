const arg0 = parseInt(process.argv[2]);

if(isNaN(arg0)){
    console.log("Please enter a valid number.");
    return;
}
for(let i = 1; i <= arg0; i++)
    console.log("&".repeat(i));
