import React from "react";
import DataComponent from "./DataComponent.jsx";
import FormComponent from "./FormComponent.jsx";
const App = () => {

const text = "Send Get request to jsonplaceholder.typicode.com/";
    return (
        <div>
          <p>{text}</p>
          <DataComponent/>
          <FormComponent/>
        </div>
    );    
};
export default App;
