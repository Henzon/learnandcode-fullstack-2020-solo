import React, {useState} from "react";
import axios from "axios";

const DataComponent = () => {
    const [input, SetInput] = useState("");
    const [data, SetData] = useState([]);
    const [endPoint, SetEndPoint] = useState("");
    const HandleSearch = async () => {
        try{
            const result = await axios.get(`https://jsonplaceholder.typicode.com/${input}`);
            if(result.data.length)
                SetData([...result.data]);
            else
                SetData([result.data]);
            
            SetEndPoint(input);
        } catch (e) {
            SetData([]);
            SetEndPoint("");
        }
    };
    const ShowData = () => {
        const baseEndPoint = endPoint.split("/")[0];

        switch(baseEndPoint){
        case "posts" : 
            return data.map((it, index) => (
                <div key={index}>
                  <p> userID: {it.userId} </p>
                  <p> {it.title} </p>
                  <p> {it.body}  </p>
                </div>
            ));
        case "comments" :
            return data.map((it, index) => (
                <div key={index}>
                  <p> postID: {it.postId} </p>
                  <p> {it.name} </p>
                  <p> {it.email} </p>
                  <p> {it.body} </p>
                </div>
            ));
        case "albums" :
            return data.map((it, index) => (
                <div key={index}>
                  <p> userID: {it.userId} </p>
                  <p> {it.title} </p>
                </div>
            ));
        case "photos" :
            return data.map((it, index) => (
                <div key={index}>
                  <p> albumID: {it.albumId} </p>
                  <p> {it.title} </p>
                  <p> {it.url} </p>
                  <p> {it.thumbnailUrl} </p>
                </div>
            ));
        case  "todos" :
            return data.map((it, index) => (
                <div key={index}>
                  <p> userID: {it.userId} </p>
                  <p> {it.title} </p>
                  <p> {it.completed ? "Completed" : "Not Completed"}  </p>
                </div>
            ));
        case "users" :
            return data.map((it, index) => (
                <div key={index}>
                  <p> Name : {it.name} </p>
                  <p> UserName : {it.username} </p>
                  <p> Email : {it.email} </p>
                  <p> Address: </p>
                  <p> {it.address.street} - {it.address.suite} </p> 
                  <p> {it.address.city} :: {it.address.zipcode}  </p>
                  <p> Phone : {it.phone} </p>
                  <p> Website : {it.website} </p>           
                </div>
            ));
        default :
            break;
        };
    }
    
    return (
        <div>
          <input onChange = {(e) => SetInput(e.target.value)}/>
            <button onClick = {HandleSearch}>
              Search
            </button>
            {ShowData()}
            <br/>
        </div>
    ); 
}

export default DataComponent;
