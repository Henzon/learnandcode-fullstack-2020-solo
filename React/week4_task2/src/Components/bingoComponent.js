import React, {useState} from "react";
import "../CSS/styleSheet.css"
const BingoComponent = (props) => {
    const {
        maxNumbers
    } = props;
    
    const [bingoNumbers, SetBingoNumbers] = useState([]);
    const [sorted, SetSorted] = useState(false);

    const AddNumber = () => {
        if(bingoNumbers.length === maxNumbers) return;
        let number;
        const IsNumberUsed = () => {return bingoNumbers.find((it) => it === number)}
        
        while (true) {
            number = Math.floor(Math.random() * maxNumbers) + 1;
            if(!IsNumberUsed()) break;
        } 
        SetBingoNumbers([...bingoNumbers, number]);
    };

    const HandleSortButton = () => {
        SetSorted(!sorted);
    };
    const ShowNumbers = () => {
        console.log(sorted);
        const numbers = [...bingoNumbers];
        if(sorted) numbers.sort((a,b) => (a > b));
        return (
                <div >
                {numbers.map((it, index) => (
                        <span className="bingoBall">
                        <p className="bingoBallText"> {it} </p>
                    </span>
                        
                ))}
            </div>
        )
    };
    
    return (
            <div>
            <h1>
        This Is Bingo (Max Numbers :: {maxNumbers})
        </h1>
            <button onClick={() => AddNumber()}> Add Number </button>
            <button onClick={() => HandleSortButton()}> Sort Numbers </button>
            <button onClick={() => {SetBingoNumbers([])}}> Reset </button>
            {ShowNumbers()}
        </div>
    );
};
export default BingoComponent;
