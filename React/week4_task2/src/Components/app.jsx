import React from "react";
import BingoComponent from "./bingoComponent.js";

const App = () => (
        <div>
        <BingoComponent maxNumbers={75} />
        </div>
);
export default App;