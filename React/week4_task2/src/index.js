import ReactDOM from "react-dom";
import App from "./Components/app.jsx";

ReactDOM.render(<App />, document.getElementById("root"));
