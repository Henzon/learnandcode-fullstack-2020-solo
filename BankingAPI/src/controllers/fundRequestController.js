import AccountModel from "../models/accountModel.js";
import FundRequestModel from "../models/fundRequestModel.js";
import HTTPStatusCode from "../httpStatusCodes.js";

async function GenerateValidRequestID() {
    const newID = Math.floor((Math.random() * 1000000));
    const found = await FundRequestModel.findOne({ request_id: newID }).exec();
    // Would rather do this in a loop but eslint doesn't like await in loops...
    if (!found) return newID;
    return GenerateValidRequestID();
}

export async function RequestFundsOrFail(req, res) {
    const amount = parseInt(req.body.amount, 10);
    if (Number.isNaN(amount) || amount <= 0) {
        res.status(HTTPStatusCode.Bad_Request).send(undefined);
        return;
    }
    const issuerFilter = {
        id: parseInt(req.body.issuer_id, 10),
        password: req.body.password,
    };
    const responderFilter = {
        id: parseInt(req.body.responder_id, 10),
    };
    const issuer = await AccountModel.findOne(issuerFilter).exec();
    const responder = await AccountModel.findOne(responderFilter).exec();

    if (issuer && responder) {
        const request = new FundRequestModel({
            request_id: await GenerateValidRequestID(),
            issuer_id: issuer.id,
            responder_id: responder.id,
            request_amount: amount,
        });
        await request.save();
        res.status(HTTPStatusCode.OK).send(request);
    } else {
        res.status(HTTPStatusCode.Not_Found).end();
    }
}
export async function GetFundRequestsOrFail(req, res) {
    // We should only get request that are from or for this user
    const accountFilter = {
        id: parseInt(req.params.user_id, 10),
        password: req.body.password,
    };
    const account = await AccountModel.findOne(accountFilter).exec();
    if (account) {
        const { filter } = req.body;
        for (let i = 0; i < Object.keys(filter).length; i += 1) filter[i] = parseInt(filter[i], 10);

        const requests = await FundRequestModel.find(filter).exec();
        if (requests) {
            // We should only get request that are from or for this user
            const filteredRequests = requests.filter((it) => (it.issuer_id === account.id ||
                                                              it.responder_id === account.id));
            res.status(HTTPStatusCode.OK).send(filteredRequests);
        } else {
            res.status(HTTPStatusCode.Not_Found).send(undefined);
        }
    } else {
        res.status(HTTPStatusCode.Not_Found).send(undefined);
    }
}
export async function AcceptFundRequestOrFail(req, res) {
    const requestID = parseInt(req.body.request_id, 10);
    if (Number.isNaN(requestID)) {
        res.status(HTTPStatusCode.Bad_Request).end();
        return;
    }

    const accountFilter = {
        id: parseInt(req.params.user_id, 10),
        password: req.body.password,
    };
    const account = await AccountModel.findOne(accountFilter).exec();
    if (account) {
        const request = await FundRequestModel.findOne({
            request_id: requestID,
            responder_id: account.id,
        }).exec();
        if (request) {
            const issuer = await AccountModel.findOne({ id: request.issuer_id }).exec();
            if (issuer && account.balance >= request.request_amount) {
                account.balance -= request.request_amount;
                issuer.balance += request.request_amount;

                await FundRequestModel.findOneAndRemove(
                    { request_id: request.request_id },
                    { useFindAndModify: false },
                ).exec();

                await AccountModel.findOneAndUpdate(
                    { id: account.id },
                    { balance: account.balance },
                    { useFindAndModify: false, new: true },
                ).exec();

                await AccountModel.findOneAndUpdate(
                    { id: issuer.id },
                    { balance: issuer.balance },
                    { useFindAndModify: false },
                ).exec();

                res.status(HTTPStatusCode.OK).json({ newBalance: account.balance });
            } else {
                res.status(HTTPStatusCode.Not_Found).send(undefined);
            }
        } else {
            res.status(HTTPStatusCode.Not_Found).send(undefined);
        }
    } else {
        res.status(HTTPStatusCode.Not_Found).send(undefined);
    }
}
export async function DeclineFundRequestOrFail(req, res) {
    const requestID = parseInt(req.body.request_id, 10);
    if (Number.isNaN(requestID)) {
        res.status(HTTPStatusCode.Bad_Request).end();
        return;
    }

    const accountFilter = {
        id: parseInt(req.params.user_id, 10),
        password: req.body.password,
    };
    const account = await AccountModel.findOne(accountFilter).exec();
    if (account) {
        const request = await FundRequestModel.findOne({
            request_id: requestID,
            responder_id: account.id,
        }).exec();
        if (request) {
            const ret = await FundRequestModel.findOneAndRemove(
                { request_id: request.request_id },
                { useFindAndModify: false },
            ).exec();

            res.status(HTTPStatusCode.OK).json(ret);
        } else {
            res.status(HTTPStatusCode.Not_Found).send(undefined);
        }
    } else {
        res.status(HTTPStatusCode.Not_Found).send(undefined);
    }
}
