import AccountModel from "../models/accountModel.js";
import HTTPStatusCode from "../httpStatusCodes.js";

async function GenerateValidID() {
    const newID = Math.floor((Math.random() * 1000000));
    const found = await AccountModel.findOne({ id: newID }).exec();
    // Would rather do this in a loop but eslint doesn't like await in loops...
    if (!found) return newID;
    return GenerateValidID();
}
export async function CreateAccountOrFail(req, res) {
    const { name, password, balance } = req.body;
    const account = new AccountModel({
        name,
        password,
        balance,
        id: await GenerateValidID(),
    });
    await account.save();
    res.status(HTTPStatusCode.OK).json(account);
}
export async function GetAccountBalanceOrFail(req, res) {
    const filter = {
        id: parseInt(req.params.user_id, 10),
        password: req.body.password,
    };
    const account = await AccountModel.findOne(filter).exec();
    if (account) {
        res.status(HTTPStatusCode.OK).json(account.balance);
    } else {
        res.status(HTTPStatusCode.Not_Found).json(undefined);
    }
}
export async function WithdrawFromAccountOrFail(req, res) {
    const amount = parseInt(req.body.amount, 10);
    if (Number.isNaN(amount) || amount <= 0) {
        res.status(HTTPStatusCode.Bad_Request).send(undefined);
        return;
    }
    const filter = {
        id: parseInt(req.params.user_id, 10),
        password: req.body.password,
    };
    const account = await AccountModel.findOne(filter).exec();
    if (account && account.balance >= amount) {
        const updatedAccount = await AccountModel.findOneAndUpdate(
            filter,
            { balance: account.balance - amount },
            { useFindAndModify: false, new: true },
        );
        res.status(HTTPStatusCode.OK).json(updatedAccount.balance);
    } else {
        res.status(HTTPStatusCode.Not_Found).json(undefined);
    }
}
export async function DepositToAccountOrFail(req, res) {
    const amount = parseInt(req.body.amount, 10);
    if (Number.isNaN(amount) || amount <= 0) {
        res.status(HTTPStatusCode.Bad_Request).send(undefined);
        return;
    }
    const filter = {
        id: parseInt(req.params.user_id, 10),
        password: req.body.password,
    };
    const account = await AccountModel.findOne(filter).exec();
    if (account) {
        const updatedAccount = await AccountModel.findOneAndUpdate(
            filter,
            { balance: account.balance + amount },
            { useFindAndModify: false, new: true },
        );
        res.status(HTTPStatusCode.OK).json(updatedAccount.balance);
    } else {
        res.status(HTTPStatusCode.Not_Found).json(undefined);
    }
}
export async function TransferToAccountOrFail(req, res) {
    const amount = parseInt(req.body.amount, 10);
    if (Number.isNaN(amount) || amount <= 0) {
        res.status(HTTPStatusCode.Bad_Request).send(undefined);
        return;
    }
    const filter = {
        id: parseInt(req.params.user_id, 10),
        password: req.body.password,
    };
    const recipientFilter = {
        id: req.body.recipient_id,
    };
    const account = await AccountModel.findOne(filter).exec();
    const recipient = await AccountModel.findOne(recipientFilter).exec();
    if (account && account.balance >= amount && recipient) {
        const updatedAccount = await AccountModel.findOneAndUpdate(
            filter,
            { balance: account.balance - amount },
            { useFindAndModify: false, new: true },
        );

        await AccountModel.findOneAndUpdate(
            recipientFilter,
            { balance: recipient.balance + amount },
            { useFindAndModify: false, new: true },
        );

        res.status(HTTPStatusCode.OK).json(updatedAccount.balance);
    } else {
        res.status(HTTPStatusCode.Not_Found).send(undefined);
    }
}
export async function ChangeAccountHolderInfoOrFail(req, res) {
    const filter = {
        id: parseInt(req.params.user_id, 10),
        password: req.body.password,
    };
    const account = await AccountModel.findOne(filter).exec();
    if (account) {
        const newData = {
            name: (req.body.newName) ? req.body.newName : account.name,
            password: (req.body.newPassword) ? req.body.newPassword : account.password,
        };
        await AccountModel.findOneAndUpdate(
            filter,
            newData,
            { useFindAndModify: false, new: true },
        );

        res.status(HTTPStatusCode.OK).json(newData);
    } else {
        res.status(HTTPStatusCode.Not_Found).send(undefined);
    }
}
export async function DeleteAccountOrFail(req, res) {
    const filter = {
        id: parseInt(req.params.user_id, 10),
        password: req.body.password,
    };
    const account = await AccountModel.findOneAndRemove(filter,
        {
            useFindAndModify: false,
            new: true,
        }).exec();
    if (account) {
        res.status(HTTPStatusCode.OK).json(account);
    } else {
        res.status(HTTPStatusCode.Not_Found).json(undefined);
    }
}
