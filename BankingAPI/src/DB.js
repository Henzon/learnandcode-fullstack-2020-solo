import mongoose from "mongoose";

const DBurl = "mongodb://localhost:27017/bankdb";
async function ConnectToDB() {
    try {
        await mongoose.connect(DBurl, { useNewUrlParser: true, useUnifiedTopology: true });
        return 1;
    } catch (e) {
        console.log(e);
        return 0;
    }
}
export default ConnectToDB;
