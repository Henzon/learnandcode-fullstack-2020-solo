import express from "express";

import {
    CreateAccountOrFail,
    GetAccountBalanceOrFail,
    WithdrawFromAccountOrFail,
    DepositToAccountOrFail,
    TransferToAccountOrFail,
    ChangeAccountHolderInfoOrFail,
    DeleteAccountOrFail,
} from "../controllers/accountController.js";
import {
    RequestFundsOrFail,
    GetFundRequestsOrFail,
    AcceptFundRequestOrFail,
    DeclineFundRequestOrFail,
} from "../controllers/fundRequestController.js";

const accountRouter = express.Router();

accountRouter.post("/:user_id/", CreateAccountOrFail);
accountRouter.get("/:user_id/balance/", GetAccountBalanceOrFail);
accountRouter.put("/:user_id/withdraw/", WithdrawFromAccountOrFail);
accountRouter.put("/:user_id/deposit/", DepositToAccountOrFail);
accountRouter.put("/:user_id/transfer/", TransferToAccountOrFail);
accountRouter.put("/:user_id/name/", ChangeAccountHolderInfoOrFail);
accountRouter.put("/:user_id/password/", ChangeAccountHolderInfoOrFail);
accountRouter.delete("/:user_id/", DeleteAccountOrFail);

accountRouter.post("/:user_id/fundRequest/", RequestFundsOrFail);
accountRouter.get("/:user_id/fundRequest/", GetFundRequestsOrFail);
accountRouter.delete("/:user_id/fundRequest/Accept/", AcceptFundRequestOrFail);
accountRouter.delete("/:user_id/fundRequest/Decline/", DeclineFundRequestOrFail);

export default accountRouter;
