import express from "express";
import accountRouter from "./routers/accountRouter.js";
import ConnectToDB from "./DB.js";

const app = express();
app.use(express.json());
app.use("/bank/", accountRouter);
if (ConnectToDB()) {
    app.listen(5000, () => console.log("Listening to port 5000"));
}
