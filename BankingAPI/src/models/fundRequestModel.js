import mongoose from "mongoose";

const fundRequestSchema = new mongoose.Schema({
    request_id: Number,
    issuer_id: Number,
    responder_id: Number,
    request_amount: Number,
});
const FundRequestModel = mongoose.model("fundRequests", fundRequestSchema);
export default FundRequestModel;
