import {
    GetUserInput,
    OutputString,
    GetConfirmationFromUser,
    GetValidAccountIDFromUser,
} from "./dialog.js";
import { GetCurrentAccountOrLogIn } from "./commandsAccount.js";
import {
    DoAccountsExist,
    GetAccountWithID,
} from "./programState.js";

function RequestFunds() {
    if (!DoAccountsExist()) return;
    OutputString("So you wish to request funds from someone.");
    const account = GetCurrentAccountOrLogIn();

    OutputString("Okay, Give us their account ID!");
    const destID = GetValidAccountIDFromUser(true);
    if (destID === account.id) {
        OutputString("Sorry, but you can't request funds from yourself!");
        return;
    }
    const destAccount = GetAccountWithID(destID);
    OutputString("Okay, we found an account with that ID. How much money do you want to request?");
    const run = true;
    while (run) {
        const amount0 = parseInt(GetUserInput(), 10);
        if (Number.isNaN(amount0) || amount0 < 0) {
            OutputString("Please enter a valid amount!");
        } else {
            const request = {
                id: account.id,
                amount: amount0,
            };
            destAccount.fund_requests = [...destAccount.fund_requests, request];
            OutputString(`Awesome! We'll request that amount from the user with ID ${destID}.`);
            break;
        }
    }
}
function ListFundRequests() {
    if (!DoAccountsExist()) return;
    const account = GetCurrentAccountOrLogIn();

    if (account.fund_requests.length === 0) {
        OutputString("You don't have any request pending.");
        return;
    }
    OutputString("Here's all the requests that are out for your funds.");
    account.fund_requests.forEach((request) => {
        OutputString(`User [${request.id}] is asking for ${request.amount}€.`);
    });
}
function HandleFundRequest(acc, request) {
    if (request.amount > acc.balance) {
        console.log("Unfortunately you don't have the fund for a request like this!");
        return;
    }
    const account = GetAccountWithID(acc.id);
    account.balance -= request.amount;
    const destAccount = GetAccountWithID(request.id);
    destAccount.balance += request.amount;

    console.log(`Good. Now these fund have been transfered to account with ID ${request.id}!`);

    account.fund_requests = account.fund_requests.filter((it) => (it !== request));
}
function AcceptFundRequest() {
    if (!DoAccountsExist()) return;

    const account = GetCurrentAccountOrLogIn();

    const keepProcessing = true;
    while (keepProcessing) {
        if (account.fund_requests.length === 0) {
            OutputString("You don't have any request pending.");
            return;
        }
        OutputString("So you want to accept someones fund request? Give us their ID.");
        const id = GetValidAccountIDFromUser(true);
        while (keepProcessing) {
            const request = account.fund_requests.find((req) => req.id === id);
            if (request !== undefined) {
                OutputString(`Okay, we found a reguest for your funds of ${request.amount}€.` +
                             "Type yes to accept this request.");
                if (GetConfirmationFromUser()) {
                    HandleFundRequest(account, request);
                } else {
                    const first = account.fund_requests[0];
                    account.fund_requests = account.fund_requests.concat(first);
                    const len = account.fund_requests.length;
                    account.fund_requests = account.fund_requests.slice(1, len);
                }
            } else {
                OutputString("Couldn't find any request from that account id.");
            }

            OutputString("Should we stop processing requests?");
            if (GetConfirmationFromUser()) {
                return;
            }
            if (account.fund_requests.length === 0) {
                OutputString("You don't have any request pending.");
                return;
            }
        }
    }
}
export {
    RequestFunds,
    ListFundRequests,
    AcceptFundRequest,
};
