import * as fs from "fs";
import { accountFileName } from "./constants.js";
import {
    AddNewAccount,
    GetAllAccounts,
} from "./programState.js";

export function DeSerializeOldAccounts() {
    try {
        const data = fs.readFileSync(accountFileName, { encoding: "utf-8", flags: "r" });
        const allAccounts = JSON.parse(data);
        allAccounts.forEach((it) => AddNewAccount(it));
    } catch (e) {
        console.error("!!! Failed to open account file !!!");
    }
}
export function SerializeAccounts() {
    const serializedAccounts = JSON.stringify(GetAllAccounts());
    fs.writeFileSync(accountFileName, serializedAccounts);
}
