const accountFileName = "./accounts.json";
const MiscName = "Misc";
const AccountName = "Account";
const FundsName = "Funds";
const RequestName = "Request";
const commandTypes = [MiscName, AccountName, FundsName, RequestName];

export {
    accountFileName,
    MiscName,
    AccountName,
    FundsName,
    RequestName,
    commandTypes,
};
