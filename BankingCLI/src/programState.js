let allAccounts = [];
let currentAccount;

export function GetCurrentAccount() {
    return currentAccount;
}
export function SetCurrentAccount(account) {
    currentAccount = account;
}
export function DoAccountsExist() {
    return allAccounts.length !== 0;
}
export function GetAllAccounts() {
    return allAccounts;
}
export function GetAccountWithID(id) {
    return allAccounts.find((it) => (it.id === id));
}
export function GetUnusedAccountID() {
    let maxID = 0;
    allAccounts.forEach((it) => {
        maxID = Math.max(maxID, it.id);
        // Maybe we can assume that requests wont have account ids that aren't used
        it.fund_requests.forEach((req) => {
            maxID = Math.max(maxID, req.id);
        });
    });
    maxID += 1;
    return maxID;
}
export function AddNewAccount(account) {
    allAccounts = [...allAccounts, account];
}
