import {
    GetUserInput,
    OutputString,
    GetConfirmationFromUser,
    GetValidAccountIDFromUser,
} from "./dialog.js";
import {
    AddNewAccount,
    GetAccountWithID,
    GetUnusedAccountID,
    GetCurrentAccount,
    SetCurrentAccount,
    DoAccountsExist,
} from "./programState.js";

function ValidateAccountCredentials(account) {
    OutputString("You will need to insert your password so we can validate " +
                 "it's actually you.");

    const run = true;
    while (run) {
        const pw = GetUserInput();
        if (pw !== account.password) {
            OutputString("Ah, there must be a typo. Try typing it again.");
        } else {
            SetCurrentAccount(account);
            return;
        }
    }
}
export function GetCurrentAccountOrLogIn() {
    let account = GetCurrentAccount();
    if (account === undefined) {
        OutputString("Please enter your account id:");
        const id = GetValidAccountIDFromUser(true);
        account = GetAccountWithID(id);
        ValidateAccountCredentials(account);

        OutputString(`Awesome, we validated you ${account.name}! `);
    }
    return account;
}
function CreateAccount() {
    OutputString("So you want to create a new account!");
    const newAcc = {
        name: undefined,
        password: undefined,
        id: undefined,
        balance: undefined,
        fund_requests: [],
    };
    do {
        OutputString("Let's start with the easy question. What is your name? ");
        newAcc.name = GetUserInput();
    } while (newAcc.name === "");
    OutputString(`Hey ${newAcc.name}! It's create to have you as a client.`);

    do {
        OutputString("How much cash do you want to deposit to get started with your account? " +
                     "(10€ is the minimum) ");
        newAcc.balance = parseInt(GetUserInput(), 10);
    } while (Number.isNaN(newAcc.balance) || newAcc.balance < 10);

    newAcc.id = GetUnusedAccountID();
    OutputString(`Great ${newAcc.name}! You now have an account ` +
                 `(ID: ${newAcc.id}) with balance of ${newAcc.balance}€.`);
    do {
        OutputString("We’re happy to have you as a customer, " +
                     "and we want to ensure that your money is safe with us. " +
                     "Give us a password, which gives only you the access to your account. ");
        newAcc.password = GetUserInput();
    } while (newAcc.password === "");
    AddNewAccount(newAcc);
}

function CloseAccount() {
    OutputString("Sorry, but you're stuck with us for now! :)");
}
function ModifyAccount() {
    if (!DoAccountsExist()) return;
    OutputString("Mhmm, you want to modify an accounts stored holder name.");
    const account = GetCurrentAccountOrLogIn();

    OutputString("What is the new name for the account holder? ");
    const run = true;
    while (run) {
        const newName = GetUserInput();
        if (newName === account.name) {
            OutputString("Mhmm, I'm sure this is the same name. Try typing it out again.");
        } else if (newName.length === 0) {
            OutputString("We need a name for the account holder");
        } else {
            account.name = newName;
            break;
        }
    }
    OutputString(`Ah, there we go, We'll adress you as ${account.name} from now on.`);
}
function DoesAccountExist() {
    OutputString("Mhmm, you want to check if an account with an ID exists. " +
                 "Lets do it! Give us the ID and we'll check.");
    const id = GetValidAccountIDFromUser(false);
    if (id) {
        OutputString("Awesome! This account actually exists. " +
                     "You should check with the owner that this account is actually theirs");
    }
}
function LogIn() {
    if (!DoAccountsExist()) return;

    const currentAccount = GetCurrentAccount();
    if (currentAccount !== undefined) {
        OutputString(`You're already logged in as ${currentAccount.name}.`);
        return;
    }
    OutputString("So you want to log in? Give us your ID.");
    const id = GetValidAccountIDFromUser(true);
    const account = GetAccountWithID(id);
    ValidateAccountCredentials(account);
}
function LogOut() {
    if (!DoAccountsExist()) return;
    const currentAccount = GetCurrentAccount();
    if (currentAccount === undefined) {
        OutputString("No account is currently logged in");
        return;
    }
    OutputString("Are you sure you want to log out?");
    if (GetConfirmationFromUser() === true) {
        SetCurrentAccount(undefined);
    }
}
export {
    CreateAccount,
    CloseAccount,
    ModifyAccount,
    DoesAccountExist,
    LogIn,
    LogOut,
};
