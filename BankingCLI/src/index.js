import { GetUserInput, OutputString } from "./dialog.js";
import registeredCommands from "./commands.js";
import { DeSerializeOldAccounts } from "./serialize.js";

DeSerializeOldAccounts();
OutputString("Welcome to höhö banking CLI!");
OutputString("**Hint:** You can get help with the commands by typing “help”.");

const run = true;
while (run) {
    OutputString("<< Please Enter a command >>");

    const input = GetUserInput();
    const command = registeredCommands.find((it) => it.name === input);
    if (command === undefined) {
        OutputString("No such command was found.");
    } else {
        command.cmd();
    }
}
