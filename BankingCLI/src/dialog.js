import readline from "readline-sync";
import {
    GetAccountWithID,
    GetCurrentAccount,
} from "./programState.js";

// Maybe we want to output into a file.
// Or keep a history of outputs/inputs which we can then use as input
const GetUserInput = () => {
    const account = GetCurrentAccount();

    const string = account ? `<< ${account.name} >> ` : ">> ";
    return readline.question(string);
};
const OutputString = (string) => {
    console.log(string);
};
function GetConfirmationFromUser() {
    const run = true;
    while (run) {
        const confirm = GetUserInput();
        if (confirm === "yes") {
            return true;
        }
        if (confirm === "no") {
            return false;
        }
        OutputString("Please enter yes or no!");
    }
}
function GetValidAccountIDFromUser(loop) {
    do {
        const id = parseInt(GetUserInput(), 10);
        if (Number.isNaN(id)) {
            OutputString("Please enter a number.");
        } else {
            const account = GetAccountWithID(id);
            if (account === undefined) {
                OutputString("Account was not found");
            } else {
                return id;
            }
        }
    } while (loop);
}
export {
    GetUserInput,
    OutputString,
    GetValidAccountIDFromUser,
    GetConfirmationFromUser,
};
