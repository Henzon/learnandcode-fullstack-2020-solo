import {
    MiscName,
    AccountName,
    FundsName,
    RequestName,
    commandTypes,
} from "./constants.js";
import {
    CreateAccount,
    CloseAccount,
    ModifyAccount,
    DoesAccountExist,
    LogIn,
    LogOut,
} from "./commandsAccount.js";
import {
    WithdrawFunds,
    DepositFunds,
    TransferFunds,
} from "./commandsFunds.js";
import {
    RequestFunds,
    ListFundRequests,
    AcceptFundRequest,
} from "./commandsRequests.js";
import {
    OutputString,
} from "./dialog.js";
import {
    SerializeAccounts,
} from "./serialize.js";

function ExitProgram() {
    SerializeAccounts();
    process.exit(0);
}
const registeredCommands = [
    {
        type: AccountName,
        name: "create_account",
        desc: "Opens a dialog for creating an account.",
        cmd: CreateAccount,
    },
    {
        type: AccountName,
        name: "close_account",
        desc: "Opens a dialog for closing an account.",
        cmd: CloseAccount,
    },
    {
        type: AccountName,
        name: "modify_account",
        desc: "Opens a dialog for modifying an account.",
        cmd: ModifyAccount,
    },
    {
        type: AccountName,
        name: "does_account_exist",
        desc: "Opens a dialog for checking if the account exists.",
        cmd: DoesAccountExist,
    },
    {
        type: AccountName,
        name: "log_in",
        desc: "Opens a dialog for logging in.",
        cmd: LogIn,
    },
    {
        type: AccountName,
        name: "log_out",
        desc: "Opens a dialog for logging out.",
        cmd: LogOut,
    },
    {
        type: FundsName,
        name: "withdraw_funds",
        desc: "Opens a dialog for withdrawing funds.",
        cmd: WithdrawFunds,
    },
    {
        type: FundsName,
        name: "deposit_funds",
        desc: "Opens a dialog for depositing funds.",
        cmd: DepositFunds,
    },
    {
        type: FundsName,
        name: "transfer_funds",
        desc: "Opens a dialog for transferring funds to another account.",
        cmd: TransferFunds,
    },
    {
        type: RequestName,
        name: "request_funds",
        desc: "Opens a dialog for requesting another user for funds.",
        cmd: RequestFunds,
    },
    {
        type: RequestName,
        name: "fund_requests",
        desc: "Shows all the requests for the account funds.",
        cmd: ListFundRequests,
    },
    {
        type: RequestName,
        name: "accept_fund_request",
        desc: "Opens a dialog for accepting a fund request.",
        cmd: AcceptFundRequest,
    },
    {
        type: MiscName,
        name: "exit",
        desc: "Close CLI",
        cmd: ExitProgram,
    },
];

function Help() {
    OutputString("I'm glad to help you :) Here's a list of commands you can use!");
    commandTypes.forEach((typeName) => {
        const typeArray = registeredCommands.filter((it) => it.type === typeName);
        OutputString(typeName);
        typeArray.forEach((it) => {
            OutputString(`   ${it.name} -- > ${it.desc}`);
        });
        OutputString("");
    });
}
registeredCommands[registeredCommands.length] = {
    type: MiscName,
    name: "help",
    desc: "Print command descriptions",
    cmd: Help,
};
export default registeredCommands;
