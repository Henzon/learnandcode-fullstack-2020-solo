const ForceNumerical = (e, Setter) => {
    const numerical = parseInt(e.target.value, 10);
    e.target.value = (numerical) ? `${numerical}` : "";
    Setter(e.target.value);
};

export default ForceNumerical;
