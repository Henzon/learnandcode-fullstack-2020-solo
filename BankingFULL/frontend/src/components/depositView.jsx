import React,{ useState, useContext} from "react";
import ForceNumerical from "../utility.js";
import { DepositFunds } from "../APIWrapper.js";
import AccountContext from "./accountContext.js";

const DepositView = () => {
    const [amount, SetAmount] = useState(0);
    const SetAccountDataIsDirty = useContext(AccountContext).SetAccountDataIsDirty;
    const [submitError, SetSubmitError] = useState("");

    const IssueForm = (e) => {
        e.preventDefault();
        DepositFunds(parseInt(amount,10)).then((res) => {
            SetAccountDataIsDirty(true);
            SetSubmitError("")
        }).catch(e => {SetSubmitError("Something went wrong")});
    }
    return (
        <div>
          Deposit Money
          <form onSubmit={IssueForm}>
            <div>
              <label>Amount: </label>
              <input type="text" name="amount"
                     onChange={(e) => {ForceNumerical(e,SetAmount)}}/>
            </div>
            <div>
              <button type="submit" > Deposit </button>
              <p className="errorString"> {submitError} </p>
            </div>
            
          </form>
        </div>
    )
}
export default DepositView;
