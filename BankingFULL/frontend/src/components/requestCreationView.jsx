import React,{ useState, useContext} from "react";
import ForceNumerical from "../utility.js";
import { CreateFundRequest } from "../APIWrapper.js";
import AccountContext from "./accountContext.js";

const RequestCreationView = () => {
    const [amount, SetAmount] = useState(0);
    const [responder, SetResponder] = useState(0);
    const [submitError, SetSubmitError] = useState("");
    
    const account = useContext(AccountContext).activeAccount;
    const IssueForm = (e) => {
        e.preventDefault();
        console.log("Request funds");
        CreateFundRequest(account.id, parseInt(responder, 10), parseInt(amount,10)).then((res) => {
            SetSubmitError("");
            //SetAccountDataIsDirty(true);
        }).catch(e => { SetSubmitError("Something went wrong");});
    }
    return (
        <div>
          Create Fund Request
          <form onSubmit={IssueForm}>
            <div>
              <label>Responder: </label>
              <input type="text" name="recipient"
                     onChange={(e) => {ForceNumerical(e,SetResponder)}}/>
            </div>
            <div>
              <label>Amount: </label>
              <input type="text" name="amount"
                     onChange={(e) => {ForceNumerical(e,SetAmount)}}/>
            </div>
            <div>
              <button type="submit"> Create Request </button>
              <p className="errorString"> {submitError} </p>
            </div>
            
          </form>
        </div>
    )
}
export default RequestCreationView;
