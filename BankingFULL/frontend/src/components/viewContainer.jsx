import React, { useState, useContext } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import "../css/styleSheet.css";
import NavBar from "./navBar.jsx";
import AccountContext from "./accountContext.js";
import DepositView from "./depositView.jsx";
import WithdrawView from "./withdrawView.jsx";
import TransferView from "./transferView.jsx";
import AccountModifyView from "./accountModifyView.jsx";
import RequestCreationView from "./requestCreationView.jsx";
import RequestHandlingView from "./requestHandlingView.jsx";
const viewComponentBindings = {
    "Deposit" : <DepositView/>,
    "Withdraw" : <WithdrawView/>,
    "Transfer" : <TransferView/>,
    "Modify" : <AccountModifyView/>,
    "Request" : <RequestCreationView/>,
    "Handle Requests" : <RequestHandlingView/>,
}

const ViewContainer = () => {
    const isLoggedIn = useContext(AccountContext).isLoggedIn; 
    return (
        <div className="viewContainer">
          <Router>
            <div className="navWrapper">
              {isLoggedIn && <NavBar views={viewComponentBindings} /> }               
            </div>
            <div className="viewWrapper">
              
              {isLoggedIn && Object.keys(viewComponentBindings).map((it, index) => {
                  const path = it.replace(" ", "");
                  console.log(path);
                  return (
                      <Route exact path={`/${path}`}>
                        {viewComponentBindings[it]}
                      </Route>
                  )
              })
              }
        
            </div>
            </Router>
            </div>
    );
};

export default ViewContainer;
