import React , {useEffect,useState} from "react";
import AccountBar from "./accountBar.jsx";
import ViewContainer from "./viewContainer.jsx"

import "../css/styleSheet.css";
import AccountContext from "./accountContext.js";
import { GetAccount, RenewAccessToken } from "../APIWrapper.js";

const App = () => {
    const [isLoggedIn, SetIsLoggedIn] = useState(false);
    const [activeAccount, SetActiveAccount] = useState({});
    const [accountDataIsDirty, SetAccountDataIsDirty] = useState(false);
    const [refreshInterval, SetRefreshInterval] = useState(null);
    
    const ClearState = () => {
        SetIsLoggedIn(false);
        SetActiveAccount({});
        SetAccountDataIsDirty(false);
        SetRefreshInterval(null);
    }                        
    useEffect( () => {
        async function UpdateAccount() {
            GetAccount()
                .then((account) => {
                    SetIsLoggedIn((account)? true : false);
                    SetActiveAccount(account);
                    SetAccountDataIsDirty(false);
                }).catch((e) => {
                 
                    ClearState();
                });
        }       UpdateAccount();
        SetAccountDataIsDirty(false);
    },[accountDataIsDirty]);

    useEffect( () => {
        if(isLoggedIn){
            const tenMinutes = 60 * 1000 * 10;
            const interval = setInterval(
                () => {
                    RenewAccessToken().then(() => {
                        SetAccountDataIsDirty();
                    }).catch(() => {
                        ClearState();
                    })}, tenMinutes);
            SetRefreshInterval(interval);
        }else{
            clearInterval(refreshInterval);
        }
    },[isLoggedIn]);
    
    return (
        <AccountContext.Provider value={{
                                     isLoggedIn,
                                     activeAccount,
                                     accountDataIsDirty,
                                     SetAccountDataIsDirty,
                                 }}>
          <div className="App">
            <div className="pageWrapper">
              <AccountBar />
              {<ViewContainer/>}
              
            </div>
          </div>
        </AccountContext.Provider>
    )
};


export default App;
