import React,{ useState, useContext} from "react";
import { ModifyAccount } from "../APIWrapper.js";
import AccountContext from "./accountContext.js";

const AccountModifyView = () => {
    const [name, SetName] = useState("");
    const [password, SetPassword] = useState("");
    const [submitError, SetSubmitError] = useState("");
    const SetAccountDataIsDirty = useContext(AccountContext).SetAccountDataIsDirty;
    
    const IssueForm = (e) => {
        e.preventDefault();
                
        ModifyAccount(name !== "" ? name : null,
                          password !== "" ? password : null)
            .then(() => {
                SetAccountDataIsDirty(true)
                SetSubmitError("")
            })
            .catch(e => {SetSubmitError("Something went wrong")});
    }
    return (
        <div>
          Modify Account
          <form onSubmit={IssueForm}>
            <div>
              <label>New Name: </label>
              <input type="text" name="name"
                     onChange={(e) => {SetName(e.target.value)}}/>
            </div>
            <div>
              <label>New Password: </label>
              <input type="password" name="password"
                     onChange={(e) => {SetPassword(e.target.value)}}/>
            </div>
            <div>
              <button type="submit" > Change Account details </button>
              <p className="errorString"> {submitError} </p>
            </div>
            
          </form>
        </div>
    )
}
export default AccountModifyView;
