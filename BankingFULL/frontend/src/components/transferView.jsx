import React,{ useState, useContext} from "react";
import ForceNumerical from "../utility.js";
import { TransferFunds } from "../APIWrapper.js";
import AccountContext from "./accountContext.js";

const TransferView = () => {
    const [recipient, SetRecipient] = useState(0);
    const [amount, SetAmount] = useState(0);
    const SetAccountDataIsDirty = useContext(AccountContext).SetAccountDataIsDirty;
    const [submitError, SetSubmitError] = useState("");

    
    const IssueForm = (e) => {
        e.preventDefault();
        console.log(e.target);
        TransferFunds(parseInt(recipient, 10), parseInt(amount,10)).then((res) => {
            SetAccountDataIsDirty(true);
            SetSubmitError("");
        }).catch(e => {SetSubmitError("Something went wrong")});
    }
    return (
        <div>
          Transfer Money
          <form onSubmit={IssueForm}>
            <div>
              <label>Recipient: </label>
              <input type="text"  name="recipient"
                     onChange={(e) => {ForceNumerical(e,SetRecipient)}}/>
            </div>
            <div>
              <label>Amount: </label>
              <input type="text" name="amount"
                     onChange={(e) => {ForceNumerical(e,SetAmount)}}/>
            </div>
            <div>
              <button type="submit"> Transfer </button>
              <p className="errorString"> {submitError} </p>
            </div>
            
          </form>
        </div>
    )
}
export default TransferView;
