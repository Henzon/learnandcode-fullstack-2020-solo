import React, { useState, useContext } from "react";
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";
import "../css/styleSheet.css";
import AccountContext from "./accountContext.js";
import ForceNumerical from "../utility.js";
import {
    Login,
    Logout,
    SignUp,
} from "../APIWrapper.js";

const AccountBanner = () => {
    const context = useContext(AccountContext);
    const LogoutOnClick = (e) => {
        e.preventDefault();
        Logout().then(() => {
            context.SetAccountDataIsDirty(true);
            
        }).catch(() => {} );
    }
    return (
        <div className="accountInfoWrapper">
          <div>
            <h2> Account Info </h2>
            <p>Name : {context.activeAccount.name}</p>
            <p>Account ID : {context.activeAccount.id} </p>
            <p>Balance : {`${context.activeAccount.balance} €`}</p>
          </div>
          <button onClick={(e) => LogoutOnClick(e)}>
               Log out </button>
        </div>
    )
}
const AccountBar = () => {
    const context = useContext(AccountContext);
    return (
        <div className="accountBar">
          {context.isLoggedIn ? <AccountBanner/> : <LogInOrOut/> }
        </div>
    );
};
const LoginView  = () => {
    const [userID, SetUserID] = useState("");
    const [password, SetPassword] = useState("");
    const SetAccountDataIsDirty = useContext(AccountContext).SetAccountDataIsDirty;
   
    const OnLoginClick = (e) => {
        e.preventDefault();
        Login(userID,password)
            .then(() => {
                SetAccountDataIsDirty(true);
            })
            .catch((e) => {});
    }
    return (
        <div>
          <h3> Login </h3>

          <form onSubmit={OnLoginClick}>
            <div>
              <label> User ID: </label>
              <input type="text" name="ID"
                     onChange={(e) => {SetUserID(e.target.value)}}/>
            </div>
            <div>
              <label> Password: </label>
              <input type="password"
                     name="password"
                     onChange={(e) => {SetPassword(e.target.value)}}/>
            </div>
            <div>
              <button type="submit" className="formButton"> Log In </button>
            </div>
          </form>
        </div>
    )
}
const SignUpView  = () => {
    const SetAccountDataIsDirty = useContext(AccountContext).SetAccountDataIsDirty;
    
    const [name, SetName] = useState("");
    const [password, SetPassword] = useState("");
    const [balance, SetBalance] = useState("");
    const OnSignUpClick = (e) => {
        e.preventDefault();
        SignUp(name,password, balance)
            .then(() => SetAccountDataIsDirty(true))
            .catch((e) => {});
    }
    return (
        <div>
          <h3> Sign Up </h3>

          <form onSubmit={OnSignUpClick}>
            <div>
              <label> Name: </label>
              <br/>
              <input type="text" name="Name"
                     onChange={(e) => {SetName(e.target.value)}}/>
            </div>
            <div>
              <label> Password: </label>
              <br/>
              <input type="password"
                     name="password"
                     onChange={(e) => {SetPassword(e.target.value)}}/>
            </div>
            <div>
              <label> Balance: </label>
              <br/>
              <input type="text" name="balance"
                     onChange={(e) => {ForceNumerical(e, SetBalance)}}/>
            </div>
            <div>
              <button type="submit" className="formButton">  Sign Up </button>
            </div>
          </form>
        </div>
    )  
}

const LogInOrOut = () => (
    <Router>

      <Route exact path="/" render = {() => (<LoginView/> )} />
        <Route exact path="/signup" render = {() => (<SignUpView /> )}/>
        <Link to="/">Login</Link> {" "}
        <Link to="/signup">Sign Up</Link>
        
    </Router>
)

export default AccountBar;
