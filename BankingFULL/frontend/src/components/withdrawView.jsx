import React,{ useState, useContext} from "react";
import ForceNumerical from "../utility.js";
import { WithdrawFunds } from "../APIWrapper.js";
import AccountContext from "./accountContext.js";

const WithdrawView = () => {
    const [amount, SetAmount] = useState(0);
    const SetAccountDataIsDirty = useContext(AccountContext).SetAccountDataIsDirty;
        const [submitError, SetSubmitError] = useState("");
    const IssueForm = (e) => {
        e.preventDefault();
        WithdrawFunds(parseInt(amount,10)).then((res) => {
            SetAccountDataIsDirty(true);
            SetSubmitError("")
        }).catch(e => {SetSubmitError("Something went wrong")});
    }
    return (
        <div>
          Withdraw Money
          <form onSubmit={IssueForm}>
            <div>
              <label>Amount: </label>
              <input type="text" name="amount"
                     onChange={(e) => {ForceNumerical(e,SetAmount)}}/>
            </div>
            <div>
              <button type="submit"> Withdraw </button>
              <p className="errorString"> {submitError} </p>
            </div>
            
          </form>
        </div>
    )
}
export default WithdrawView;
