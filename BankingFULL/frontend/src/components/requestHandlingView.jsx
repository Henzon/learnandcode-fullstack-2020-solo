import React,{ useState, useContext, useEffect} from "react";
import AccountContext from "./accountContext.js";
import {
    GetAllRequestsFor,
    AcceptRequestWithID,
    DeclineRequestWithID,
} from "../APIWrapper.js";
const RequestHandlingView = () => {

    //Get all request and list them here
    const [refresh, SetRefresh] = useState(true);
    const [requests, SetRequests] = useState([]);
    const [errorString, SetErrorString] = useState("");
    const [errCount, SetErrCount] = useState(0);
    const SetAccountDataIsDirty = useContext(AccountContext).SetAccountDataIsDirty;
    
    useEffect(() => {
        const GetRequests = async () => {
            GetAllRequestsFor().then((reqs) => {
                SetRequests(reqs);
            }).catch(e => {SetErrorString("Something went wrong")});
            SetRefresh(false);
        };
        if(refresh)
            GetRequests();
        
    },[refresh])
    useEffect(()=> {console.log("redraw");} , []);
    const AcceptRequest = (req) => {
        AcceptRequestWithID(req.request_id)
            .then(res =>{
                SetAccountDataIsDirty(true);
                const newReqs = requests.filter((it) => it.request_id !== req.request_id);
                SetRequests(newReqs);
            }).catch(e => {
                const request = requests.find(it => it.request_id === req.request_id);
                SetErrCount(errCount + 1);
            }
           );
    }
    const DeclineRequest = (req) => {
        DeclineRequestWithID(req.request_id)
            .then(res => {
                const newReqs = requests.filter((it) => it.request_id !== req.request_id);
                SetRequests(newReqs);
            }).catch(e => {});
    }
    
    return (
        <div>
          <button onClick={(e) => SetRefresh(true)}> Refresh </button>
          {errorString.length !== 0 && <p className="errorString">{errorString}</p>}
          <br/>
          <div className="requestListingWrapper">
            {requests.map((it) => {
                return(
                    <div key={it.request_id} className="requestWrapper">
                      <h3> # {it.request_id} </h3>
                      <p> Issuer: {it.issuer_id} </p>
                      <p> Amount: {it.request_amount} € </p>
                      <button onClick={(e) => DeclineRequest(it)}> Decline </button>
                      <button onClick={(e) => AcceptRequest(it)}> Accept </button>
                      <p className="errorString">{it.error}</p>
                    </div>
                )})
            }
        </div>
          
        </div>
    )
}

export default RequestHandlingView;
