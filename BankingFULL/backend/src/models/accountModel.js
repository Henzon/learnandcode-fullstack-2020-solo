import mongoose from "mongoose";

const accountSchema = new mongoose.Schema({
    name: String,
    id: Number,
    balance: Number,
    password: String,
});
const AccountModel = mongoose.model("account", accountSchema);
export default AccountModel;
