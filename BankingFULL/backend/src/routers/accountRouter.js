import express from "express";
import {
    Login, Logout,
    CreateAccountOrFail,
    GetAccountBalanceOrFail,
    WithdrawFromAccountOrFail,
    DepositToAccountOrFail,
    TransferToAccountOrFail,
    ChangeAccountHolderInfoOrFail,
    DeleteAccountOrFail,
} from "../controllers/accountController.js";
import {
    RequestFundsOrFail,
    GetFundRequestsOrFail,
    AcceptFundRequestOrFail,
    DeclineFundRequestOrFail,
} from "../controllers/fundRequestController.js";
import {
    AuthenticateLocal,
    AuthenticateAccessToken,
    AuthenticateRefreshToken,
    RenewAccessToken,
} from "../authentication.js";

const accountRouter = express.Router();

accountRouter.post("/signup/", CreateAccountOrFail);
accountRouter.post("/login", AuthenticateLocal, Login);
accountRouter.post("/logout", Logout);
accountRouter.post("/refresh", AuthenticateRefreshToken, RenewAccessToken);

accountRouter.get("/:user_id/",
    AuthenticateAccessToken,
    (req, res) => res.status(200).json(req.body.account));
accountRouter.get("/:user_id/balance/",
    AuthenticateAccessToken,
    GetAccountBalanceOrFail);

accountRouter.put("/:user_id/withdraw/",
    AuthenticateAccessToken,
    WithdrawFromAccountOrFail);

accountRouter.put("/:user_id/deposit/",
    AuthenticateAccessToken,
    DepositToAccountOrFail);

accountRouter.put("/:user_id/transfer/",
    AuthenticateAccessToken,
    TransferToAccountOrFail);

accountRouter.put("/:user_id/modify/",
    AuthenticateAccessToken,
    ChangeAccountHolderInfoOrFail);

accountRouter.delete("/:user_id/",
    AuthenticateAccessToken,
    DeleteAccountOrFail);

// ##################
// FUNDS
// ##################
accountRouter.post("/:user_id/fundRequest/",
    AuthenticateAccessToken,
    RequestFundsOrFail);

accountRouter.get("/:user_id/fundRequest/",
    AuthenticateAccessToken,
    GetFundRequestsOrFail);

accountRouter.delete("/:user_id/fundRequest/accept/",
    AuthenticateAccessToken,
    AcceptFundRequestOrFail);

accountRouter.delete("/:user_id/fundRequest/decline/",
    AuthenticateAccessToken,
    DeclineFundRequestOrFail);

export default accountRouter;
