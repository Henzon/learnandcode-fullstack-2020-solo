import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import AccountModel from "./models/accountModel.js";
import HTTPStatusCodes from "./httpStatusCodes.js"
const secretKey = "mysecretkey";
const refreshSecretKey = "mysecretkey";
const AT_EXPIRE_TIME = "15min";
const RT_EXPIRE_TIME = "1h";

export const CreateTokens = (userID) => (
    {
        token: jwt.sign({ userID }, secretKey, { expiresIn: AT_EXPIRE_TIME }),
        refreshToken: jwt.sign({ userID }, refreshSecretKey, { expiresIn: RT_EXPIRE_TIME }),
    }
);
export const RenewAccessToken = (req, res) => {
    // Renew access token while refresh token is valid.
    const tokens = CreateTokens(req.body.decoded.userID);
    res.status(HTTPStatusCodes.OK).json({ token: tokens.token });
};
export const AuthenticateLocal = (req, res, next) => {
    if (!req.body.userID || !req.body.password) {
        return res.status(HTTPStatusCodes.Forbidden).json({ Error: "Not Authorized" });
    }
    
    AccountModel.findOne({ id: req.body.userID }).then(async (it) => {
        if (!it) return res.status(HTTPStatusCodes.Forbidden).json({ Error: "Not Found" });
        const isMatch = bcrypt.compareSync(req.body.password, it.password);
        if (!isMatch) return res.status(HTTPStatusCodes.Forbidden).json({ Error: "Not Authorized" });
        
        next();
    });
    return;
};
export const AuthenticateAccessToken = (req, res, next) => {
    if (!req.headers.authentication) return res.status(HTTPStatusCodes.Forbidden).json({ Error: "NoToken" });
    const token = req.headers.authentication.split(" ")[1];
    jwt.verify(token, secretKey, (err, decoded) => {
        if (err) {
            return res.status(HTTPStatusCodes.Forbidden).json(err);
        }
        AccountModel.findOne({ id: decoded.userID }).then(async (it) => {
            if (!it) return res.status(HTTPStatusCodes.Forbidden).json({ Error: "Account Not Found" });

            req.body = { ...req.body, decoded, account: it };
            next();
        });
    });
};
export const AuthenticateRefreshToken = (req, res, next) => {
    const {
        cookies,
    } = req;
    jwt.verify(cookies.refreshToken, refreshSecretKey, (err, decoded) => {
        if (err) {
            return res.status(HTTPStatusCodes.Forbidden).json(err);
        }
        req.body = { ...req.body, decoded };
    });
    next();
};
